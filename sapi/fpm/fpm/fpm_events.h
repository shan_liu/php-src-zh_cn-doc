
	/* $Id: fpm_events.h,v 1.9 2008/05/24 17:38:47 anight Exp $ */
	/* (c) 2007,2008 Andrei Nigmatulin */

#ifndef FPM_EVENTS_H
#define FPM_EVENTS_H 1

#define FPM_EV_TIMEOUT  (1 << 0) //由超时事件触发
#define FPM_EV_READ     (1 << 1) //读取事件
#define FPM_EV_PERSIST  (1 << 2) //持续,没有这标记执行一次超时后时间移除
#define FPM_EV_EDGE     (1 << 3)
//超时回调fpm_event_s结构填充,不会添加到多路复用处理中
#define fpm_event_set_timer(ev, flags, cb, arg) fpm_event_set((ev), -1, (flags), (cb), (arg))
//事件注册结构
struct fpm_event_s {
	int fd;                   /* not set with FPM_EV_TIMEOUT *///用于master进程事件循环的句柄
	struct timeval timeout;   /* next time to trigger */
	struct timeval frequency;
	void (*callback)(struct fpm_event_s *, short, void *);
	void *arg;
	int flags;
	int index;                /* index of the fd in the ufds array */
	short which;              /* type of event */
};
//事件队列结构
typedef struct fpm_event_queue_s {
	struct fpm_event_queue_s *prev;
	struct fpm_event_queue_s *next;
	struct fpm_event_s *ev;
} fpm_event_queue;
//事件用IO多路复用模块结构
struct fpm_event_module_s {
	const char *name;
	int support_edge_trigger;
	int (*init)(int max_fd);
	int (*clean)(void);
	int (*wait)(struct fpm_event_queue_s *queue, unsigned long int timeout);
	int (*add)(struct fpm_event_s *ev);
	int (*remove)(struct fpm_event_s *ev);
};

void fpm_event_loop(int err);
void fpm_event_fire(struct fpm_event_s *ev);
int fpm_event_init_main();
int fpm_event_set(struct fpm_event_s *ev, int fd, int flags, void (*callback)(struct fpm_event_s *, short, void *), void *arg);
int fpm_event_add(struct fpm_event_s *ev, unsigned long int timeout);
int fpm_event_del(struct fpm_event_s *ev);
int fpm_event_pre_init(char *machanism);//选择和设置事件处理的IO复用模型
const char *fpm_event_machanism_name();
int fpm_event_support_edge_trigger();

#endif
